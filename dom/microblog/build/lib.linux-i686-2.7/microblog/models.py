# -*- coding: utf-8 -*-

from persistent.mapping import PersistentMapping
from persistent import Persistent


class Blog(PersistentMapping):
    """ Klasa dla kontenera """
    # Należy ją odpowiednio zainicjalizować
    __name__ = None
    __parent__ = None


class Post(Persistent):
    """ Klasa dla pojedynczego wpisu """
    def __init__(self, title, content, created):
        self.title = title
        self.content = content
        self.created = created


def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        # Utworzenie głównegp kontenera
        app_root = Blog()
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
