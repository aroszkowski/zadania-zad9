# -*- coding: utf-8 -*-

import unittest

from pyramid import testing
import datetime

####################
#   Testy modeli   #
####################
class BlogModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Blog
        return Blog

    def _makeOne(self):
        return self._getTargetClass()()

    def test_it(self):
        wiki = self._makeOne()
        self.assertEqual(wiki.__parent__, None)
        self.assertEqual(wiki.__name__, None)

class PostModelTests(unittest.TestCase):
    time = datetime.datetime.now()
    def _getTargetClass(self):
        from .models import Post
        return Post

def _makeOne(self, data=u'some data'):

    return self._getTargetClass()(title='test', content='test', created =self.time )

def test_constructor(self):
    instance = self._makeOne()
    self.assertEqual(instance.title, 'test')
    self.assertEqual(instance.title, 'test')
    self.assertEqual(instance.title, self.time)



#####################
#   Testy widoków   #
#####################


class ViewBlogTests(unittest.TestCase):
    def test_it(self):
        from .views import view_blog
        from .models import Blog
        context = testing.DummyResource()
        request = testing.DummyRequest()
        response = view_blog(Blog(), request)
        self.assertTrue(response!=None)
class AddPostTest(unittest.TestCase):
    pass

# class ViewPageTests(unittest.TestCase):
#     def _callFUT(self, context, request):
#         from .views import view_blog
#         return view_blog(context, request)
#
#     def test_it(self):
#         wiki = testing.DummyResource()
#         wiki[u'NaprawdęIstnieje'] = testing.DummyResource()
#         context = testing.DummyResource(data=u'Witaj, OkropnyŚwiat NaprawdęIstnieje')
#         context.__parent__ = wiki
#         context.__name__ = 'strona'
#         request = testing.DummyRequest()
#         info = self._callFUT(context, request)
#         self.assertEqual(info['page'], context)
#         self.assertEqual(
#             info['content'],
#             u'<div class="document">\n'
#             u'<p>Witaj, <a href="http://example.com/add_page/Okropny%C5%9Awiat">'
#             u'OkropnyŚwiat</a> '
#             u'<a href="http://example.com/Naprawd%C4%99Istnieje/">'
#             u'NaprawdęIstnieje</a>'
#             u'</p>\n</div>\n')
#         self.assertEqual(info['edit_url'],
#                          'http://example.com/strona/edit_page')
#
#
# class AddPageTests(unittest.TestCase):
#     def _callFUT(self, context, request):
#         from .views import add_post
#         return add_post(context, request)
#
#     def test_it_notsubmitted(self):
#         context = testing.DummyResource()
#         request = testing.DummyRequest()
#         request.subpath = ['InnaStrona']
#         info = self._callFUT(context, request)
#         self.assertEqual(info['page'].data,'')
#         self.assertEqual(
#             info['save_url'],
#             request.resource_url(context, 'add_page', 'InnaStrona'))
#
#     def test_it_submitted(self):
#         context = testing.DummyResource()
#         data = {'form.submitted':True, 'body':'Siemanko!'}
#         request = testing.DummyRequest(data, post=data)
#         request.subpath = ['AnotherPage']
#         self._callFUT(context, request)
#         page = context['AnotherPage']
#         self.assertEqual(page.data, 'Siemanko!')
#         self.assertEqual(page.__name__, 'AnotherPage')
#         self.assertEqual(page.__parent__, context)
