# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
import datetime
from .models import Blog, Post


# Główna strona
@view_config(context='.models.Blog', renderer='templates/list.pt')
def view_blog(context, request):
    """ Wyświetla listę n ostatnich wpisów """
    blog = context
    add_url = request.resource_url(context, 'add')
    return  dict(add_url=add_url, blog=blog)


@view_config(context='.models.Post', renderer='templates/view.pt')
def view_post(context, request):
    post = context
    return dict(post=post)


@view_config(name='add', context='.models.Blog')
def add_post(context, request):
    blog = context.__parent__
    if request.method == 'POST':
        title = request.POST['title']
        content = request.POST['content']
        # if title in blog:
        #     request.session.flash('Post istnieje')
        #     return HTTPFound(location=request.resource_url(context.__parent__))
        # else:
        created = datetime.datetime.now()
        post = Post(title,content, created)
        post.__name__= title
        post.__parent__ = context
        context[title] = post
        # request.session.flash('Dodano')
        return HTTPFound(location=request.resource_url(context.__parent__))