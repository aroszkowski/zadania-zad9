# -*- coding: utf-8 -*-

from persistent.mapping import PersistentMapping
from persistent import Persistent
from zope.interface import Interface, implements



class Blog(PersistentMapping):
    """ Klasa dla kontenera """
    # Należy ją odpowiednio zainicjalizować
    __name__ = None
    __parent__ = None


class Post(PersistentMapping):
    """ Klasa dla pojedynczego wpisu """
    def __init__(self, title, content, created):
        self.data = dict()
        self.title = title
        self.content = content
        self.created = created
class Comment(PersistentMapping):
    def __init__(self, author, text, created):
        self.data = dict()
        self.author = author
        self.text = text
        self.created = created

def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        # Utworzenie głównegp kontenera
        app_root = Blog()
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
