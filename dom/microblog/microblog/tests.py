# -*- coding: utf-8 -*-

import unittest

from pyramid import testing
import datetime

####################
#   Testy modeli   #
####################
class BlogModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Blog
        return Blog

    def _makeOne(self):
        return self._getTargetClass()()

    def test_it(self):
        wiki = self._makeOne()
        self.assertEqual(wiki.__parent__, None)
        self.assertEqual(wiki.__name__, None)

class PostModelTests(unittest.TestCase):
    time = datetime.datetime.now()
    def _getTargetClass(self):
        from .models import Post
        return Post

    def _makeOne(self):

        return self._getTargetClass()(title='test', content='test', created =self.time )

    def test_constructor(self):
        instance = self._makeOne()
        self.assertEqual(instance.title, 'test')
        self.assertEqual(instance.title, 'test')
        self.assertEqual(instance.created, self.time)

class CommentModelTests(unittest.TestCase):
    time = datetime.datetime.now()
    def _getTargetClass(self):
        from .models import Comment
        return Comment

    def _makeOne(self):
        return self._getTargetClass()(text='test', author='test', created=self.time )

    def test_constructor(self):
        instance = self._makeOne()
        self.assertEqual(instance.text, 'test')
        self.assertEqual(instance.author, 'test')
        self.assertEqual(instance.created, self.time)




#####################
#   Testy widoków   #
#####################


class ViewBlogTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import view_blog
        return view_blog(context, request)
    def test_it(self):
        def test_it(self):
                blog = testing.DummyResource()
                blog.__parent__ = None
                blog.__name__ = None
                post = testing.DummyResource()
                post[u'Test'] = testing.DummyResource()
                context = testing.DummyResource(content=u'Test', title=u'Test', created=datetime.datetime.now())
                context.__parent__ = blog
                context.__name__ = 'test'
                request = testing.DummyRequest()
                info = self._callFUT(context, request)
                self.assertEqual(info['page'], context)
                self.assertTrue(u'Test' in info['content'])
                self.assertEqual(info['add_url'],
                                 'http://example.com/test/add')

class AddandViewPostTest(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import add_post
        return add_post(context, request)
    def test_it(self):
        def test_it(self):
            context = testing.DummyResource()
            data = {'content':'test', 'title':'test'}
            request = testing.DummyRequest(data, post=data)
            self._callFUT(context,request)
            page = context['test']
            content = vars(page)
            self.assertTrue('test' in unicode(content))
            self.assertEqual(page.__name__, 'test')
            self.assertEqual(page.__parent__, context)

class ViewandAddCommentTest(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import add_comment
        return add_comment(context, request)
    def test_it(self):
        context = testing.DummyResource()
        data = {'author':'test', 'text':'test'}
        request = testing.DummyRequest(data, post=data)
        self._callFUT(context,request)
        for i in context.items():
            page = i
        for item in page:
            test = item
        self.assertTrue('test by test' in unicode(page))
        self.assertEqual(test.__parent__, context)

