# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
import datetime
from .models import Blog, Post, Comment
from docutils.core import publish_parts
import pdb
from operator import itemgetter



def comment_count(dictionary, ct):
        for key, value in dictionary.iteritems():
            ct.append(1)
            comment_count(dictionary[key],ct=ct)
        return len(ct)
def comment_print(dictionary):
    output='<ul>'
    for key, value in dictionary.iteritems():
        output+='<li>%s</li>'%key
        output+=comment_print(dictionary[key])
    output='</ul>'
    return output
# Główna strona
@view_config(context='.models.Blog', renderer='templates/list.pt')
def view_blog(context, request):
    """ Wyświetla listę n ostatnich wpisów """
    blog_list = context.items()
    blog = sorted(blog_list, key=lambda post: post[1].created, reverse=True)[:5]
    blog = dict(blog)
    add_url = request.resource_url(context, 'add')
    view_url = request.resource_url(context)
    num = {}
    for item in blog:
        num[item] = comment_count(blog[item], [])
    return  dict(add_url=add_url, blog=blog, view_url=view_url, num=num)


@view_config(context='.models.Post', renderer='templates/view.pt')
def view_post(context, request):
    post = context
    content = context.content
    add_url = request.resource_url(context, 'add')
    view_url = request.resource_url(context)
    return dict(post=post, content=content, add_url=add_url, view_url=view_url)

@view_config(context='.models.Comment', renderer='templates/comment.pt')
def view_comment(context, request):
    comment = context
    post = context.__parent__.keys()[0]
    add_url = request.resource_url(context, 'add')
    view_url = request.resource_url(context)
    return dict(comment=comment, post = post, add_url=add_url, view_url=view_url)


@view_config(name='add', context='.models.Blog')
def add_post(context, request):
    if request.method == 'POST':
        title = request.POST['title']
        content = request.POST['content']
        if title and content:
            content = toRST(content)
            created = datetime.datetime.now()
            post = Post(title, content, created)
            post.__name__ = title
            post.__parent__ = context
            context[title] = post
    return HTTPFound(location=request.resource_url(context))
@view_config(name='add', context='.models.Post')
def add_comment(context, request):
    if request.method == 'POST':
        author = request.POST['author']
        text = request.POST['text']
        if author and text:
            created = datetime.datetime.now()
            name = '%s by %s %s'%(unicode(text), unicode(author),unicode(created))
            comment = Comment(author, text, created)
            comment.__name__ = name
            comment.__parent__ = context
            context[name] = comment
    return HTTPFound(location=request.resource_url(context))
@view_config(name='add', context='.models.Comment')
def add_comment1(context, request):
    if request.method == 'POST':
        author = request.POST['author']
        text = request.POST['text']
        if author and text:
            created = datetime.datetime.now()
            name = '%s by %s %s'%(unicode(text), unicode(author),unicode(created))
            comment = Comment(author, text, created)
            comment.__name__ = name
            comment.__parent__ = context
            context[name] = comment
    return HTTPFound(location=request.resource_url(context))

def toRST(text):
    return publish_parts(text, writer_name='html')['html_body']